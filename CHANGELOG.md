# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.5

- patch: Internal maintenance: Bump dependency bitbucket-pipes-toolkit to 2.2.0 version.

## 0.4.4

- patch: Fix and add example sending notification with a build status.

## 0.4.3

- patch: Internal maintenance: refactor tests to use conventional aws creds.

## 0.4.2

- patch: Internal maintenance: fix tests.

## 0.4.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.4.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.3.12

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.11

- patch: Internal maintenance: Fix test infrastructure.

## 0.3.10

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.9

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.8

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.7

- patch: Add warning message about new version of the pipe available.

## 0.3.6

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.3.5

- patch: Added code style checks

## 0.3.4

- patch: Internal maintenance: Fix issue. Update Readme.

## 0.3.3

- patch: Internal maintenance: update pipes toolkit version

## 0.3.2

- patch: Documentation updates

## 0.3.1

- patch: Fix the bug with a default SUBJECT missing

## 0.3.0

- minor: Introduced the support for sending email notifications with attachments

## 0.2.1

- patch: Updated the internal dependency version

## 0.2.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.1.0

- minor: Initial release

